from django.urls import path

from market import views
from market.views import ItemView, WallpapersView, WallpaperItemView, WallpaperCreateView

app_name = 'market'

urlpatterns = [
    path('', WallpapersView.as_view(), name='list'),
    path('view/<item_id>/', WallpaperItemView.as_view(), name='item'),
    path('add/', WallpaperCreateView.as_view(), name='add'),
    path('wallpaper/<item_id>/edit', views.update, name='edit'),
    path('wallpaper/<item_id>/', ItemView.as_view(), name='wallpaper'),
]
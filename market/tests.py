from django.test import TestCase

# Create your tests here.
from django.urls import reverse

from market.models import Brand, Wallpaper


class TestBrand(TestCase):
    def test_str_method(self):
        test_model = Brand(name='Name', rating=4.5)
        test_model.save()
        model_str = str(test_model)
        self.assertEqual(model_str, '1-Name(4.5)')


class TestWallpaper(TestCase):
    def test_url(self):
        brand_model = Brand(name='Name', rating=4.5)
        test_model = Wallpaper(
            slug='name',
            name='Name',
            color='Color',
            description='Description',
            brand=brand_model,
        )
        self.assertEqual(test_model.get_absolute_url(), f'/market/view/{test_model.slug}/')

    def test_slug(self):
        brand_model = Brand(name='Name', rating=4.5)
        brand_model.save()
        test_model = Wallpaper(
            name='Test Name 1',
            color='Color',
            description='Description',
            brand=brand_model,
        )
        test_model.save()
        self.assertEqual(test_model.slug, 'test-name-1-color')

    def test_long_slug(self):
        brand_model = Brand(name='Name', rating=4.5)
        brand_model.save()
        test_model = Wallpaper(
            name='Test Name 1 Too many Letters',
            color='Very long Color name',
            description='Description',
            brand=brand_model,
        )
        test_model.save()
        self.assertEqual(test_model.id, 1)
        self.assertEqual(test_model.slug, 'test-name-1-too-many-very long color name')


class WallpapersViewTest(TestCase):
    def test_empty_response(self):
        response = self.client.get(reverse('market:list'))
        self.assertEqual(response.context[0]['injected_text'], 'You\'ve been hacked')
        wallpapers = response.context[0]['object_list']
        self.assertEqual(len(wallpapers), 0)
        self.assertContains(response, '0 wallpapers found')

    def test_wallpaper_response(self):
        brand_model = Brand(name='Name', rating=4.5)
        brand_model.save()
        test_model = Wallpaper(
            name='Test Name 1 Too many Letters',
            color='Very long Color name',
            description='Description',
            brand=brand_model,
        )
        test_model.save()

        response = self.client.get(reverse('market:list'))
        self.assertEqual(response.context[0]['injected_text'], 'You\'ve been hacked')
        wallpapers = response.context[0]['object_list']
        self.assertEqual(len(wallpapers), 1)
        self.assertIn(test_model, wallpapers)
        self.assertContains(response, '1 wallpapers found')

    def test_filter_available_response(self):
        brand_model = Brand(name='Name', rating=4.5)
        brand_model.save()
        test_model_1 = Wallpaper(
            name='Test Name 2 Too many Letters',
            color='Very long Color name',
            description='Description',
            brand=brand_model,
            price=10000,
            quantity=2
        )
        test_model_2 = Wallpaper(
            name='Test Name 1 Too many Letters',
            color='Very long Color name',
            description='Description',
            brand=brand_model,
            price=20000,
            available=True
        )
        test_model_1.save()
        test_model_2.save()
        filters = {
            'avilable': 1,
        }
        response = self.client.get(reverse('market:list'), data=filters)

        wallpapers = response.context[0]['object_list']
        self.assertEqual(len(wallpapers), 1)
        self.assertIn(test_model_2, wallpapers)

    def test_filter_quantity_response(self):
        brand_model = Brand(name='Name', rating=4.5)
        brand_model.save()
        test_model_1 = Wallpaper(
            name='Test Name 2 Too many Letters',
            color='Very long Color name',
            description='Description',
            brand=brand_model,
            price=10000,
            quantity=2
        )
        test_model_2 = Wallpaper(
            name='Test Name 1 Too many Letters',
            color='Very long Color name',
            description='Description',
            brand=brand_model,
            price=20000,
            available=True
        )
        test_model_1.save()
        test_model_2.save()
        filters = {
            'min-quantity': 1,
        }
        response = self.client.get(reverse('market:list'), data=filters)

        wallpapers = response.context[0]['object_list']
        self.assertEqual(len(wallpapers), 1)
        self.assertIn(test_model_1, wallpapers)

    def test_filter_price_response(self):
        brand_model = Brand(name='Name', rating=4.5)
        brand_model.save()
        test_model_1 = Wallpaper(
            name='Test Name 2 Too many Letters',
            color='Very long Color name',
            description='Description',
            brand=brand_model,
            price=10000,
            quantity=2
        )
        test_model_2 = Wallpaper(
            name='Test Name 1 Too many Letters',
            color='Very long Color name',
            description='Description',
            brand=brand_model,
            price=20000,
            available=True
        )
        test_model_1.save()
        test_model_2.save()
        filters = {
            'min-price': 15000,
        }
        response = self.client.get(reverse('market:list'), data=filters)

        wallpapers = response.context[0]['object_list']
        self.assertEqual(len(wallpapers), 1)
        self.assertIn(test_model_2, wallpapers)

    def test_all_filter_response(self):
        brand_model = Brand(name='Name', rating=4.5)
        brand_model.save()
        test_model_1 = Wallpaper(
            name='Test Name 2 Too many Letters',
            color='Very long Color name',
            description='Description',
            brand=brand_model,
            price=10000,
            quantity=2
        )
        test_model_2 = Wallpaper(
            name='Test Name 1 Too many Letters',
            color='Very long Color name',
            description='Description',
            brand=brand_model,
            price=20000,
            available=True
        )
        test_model_1.save()
        test_model_2.save()
        filters = {
            'min-price': 15000,
            'min-quantity': 0,
            'avilable': 1
        }
        response = self.client.get(reverse('market:list'), data=filters)

        wallpapers = response.context[0]['object_list']
        self.assertEqual(len(wallpapers), 1)
        self.assertIn(test_model_2, wallpapers)
        self.assertContains(response, '1 wallpapers found')

        filters = {
            'min-price': 5000,
            'min-quantity': 1,
            'avilable': 0
        }
        response = self.client.get(reverse('market:list'), data=filters)

        wallpapers = response.context[0]['object_list']
        self.assertEqual(len(wallpapers), 1)
        self.assertIn(test_model_1, wallpapers)
        self.assertContains(response, '1 wallpapers found')

        filters = {
            'min-price': 50000,
            'min-quantity': 10,
            'avilable': 1
        }
        response = self.client.get(reverse('market:list'), data=filters)

        wallpapers = response.context[0]['object_list']
        self.assertEqual(len(wallpapers), 0)
        self.assertContains(response, '0 wallpapers found')


class WallpaperDetailsTest(TestCase):
    def test_wallpaper_response(self):
        brand_model = Brand(name='Name', rating=4.5)
        brand_model.save()
        test_model = Wallpaper(
            name='Test Name 1 Too many Letters',
            color='Very long Color name',
            description='Description',
            brand=brand_model,
        )
        test_model.save()

        response = self.client.get(test_model.get_absolute_url())
        wallpaper = response.context_data['object']
        self.assertEqual(wallpaper, test_model)

    def test_wallpaper_not_found(self):
        response = self.client.get(reverse('market:item', kwargs={'item_id': 'mythical object'}))
        self.assertEqual(response.status_code, 404)
